// simulation.js

var mass_val;
var velocity_val;

const slider1 = document.getElementById("mass");
const value1 = document.querySelector("#mass_value");
value1.textContent = slider1.value;
slider1.oninput = function () {
  value1.textContent = this.value;
  mass_val = this.value;
};

const slider2 = document.getElementById("velocity");
const value2 = document.querySelector("#velocity_value");
value2.textContent = slider2.value;
slider2.oninput = function () {
  value2.textContent = this.value;
  velocity_val = this.value;
};

function startSimulation() {
  const url = `http://localhost:5173/?mass=${mass_val}&velocity=${velocity_val}`;
  window.location.href = url;
}
