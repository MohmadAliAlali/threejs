import * as THREE from 'three';
import { gsap } from 'gsap';
import { Water } from '.././Water.js';
export default class WaterEffect {
    constructor(scene, renderer, camera) {
      const waterGeometry = new THREE.PlaneGeometry(10000, 10000);

      const water = new Water(waterGeometry, {
        textureWidth: 512,
        textureHeight: 512,
        waterNormals: new THREE.TextureLoader().load('js/waternormals.jpg', function (texture) {
          texture.wrapS = texture.wrapT = THREE.RepeatWrapping;
        }),
        alpha: 1.0,
        sunDirection: new THREE.Vector3(),
        sunColor: new THREE.Color(0xff8c00),
        waterColor: new THREE.Color(0x0080ff),
        distortionScale: 1,
        fog: scene.fog !== undefined,
      });

      water.position.y = 0
      water.rotation.x = -Math.PI / 2;
      scene.add(water);
      this.water = water;


    }

  
    update(deltaTime) {
      this.water.material.uniforms.time.value += deltaTime;
    }
  }
  
