// firstPersonMovement.js
import * as THREE from 'three';
// import { cos ,sin} from 'three/examples/jsm/nodes/Nodes.js';

export default class FirstPersonMovement {
  constructor(object ) {
    this.object = object;
    this.clock = new THREE.Clock();
    this.movementSpeed = 10;
    this.rotationSpeed = 0.05; 
    this.rotateX = 0;
    const angel = 0;
    this.keyStates = {
      forward: false,
      backward: false,
      left: false,
      right: false,
      start : true,
      stop : false
    };
    this.addEventListeners();
  }

  addEventListeners() {
    document.addEventListener('keydown', this.onKeyDown.bind(this));
    document.addEventListener('keyup', this.onKeyUp.bind(this));
  }

  onKeyDown(event) {
    switch (event.keyCode) {
      case 65: // A
        this.keyStates.left = true;
        break;
      case 68: // D
        this.keyStates.right = true;
        break;
    }
  }

  onKeyUp(event) {
    switch (event.keyCode) {
      case 65: // A
        this.keyStates.left = false;
        break;
      case 68: // D
        this.keyStates.right = false;
        break;
    }
  }

  update() {
    const delta = this.clock.getDelta();
    if (this.keyStates.forward && this.movementSpeed<100) {
      this.movementSpeed +=0.01 ;
    } 
    // if (this.keyStates.stop && this.movementSpeed >=0) {
    //   this.movementSpeed -=0.01 ;
    // }
    const movementDistance = this.movementSpeed * -delta;
    const rotationAngle = this.rotationSpeed * delta;
    if (this.keyStates.start) {
      this.object.translateX(movementDistance -0.1);
    }
    if (this.keyStates.forward) {
      this.object.translateX(movementDistance);
    }
    if (this.keyStates.backward) {
      this.object.translateX(-movementDistance + 0.2);
    }
    if (this.keyStates.left && this.keyStates.forward || this.keyStates.left && this.keyStates.start && this.rotationAngle <10) {
      this.object.rotateY(rotationAngle);
    }
    if (this.keyStates.right && this.keyStates.forward || this.keyStates.right && this.keyStates.start && this.rotationAngle <10) {
      // this.rotationAngle += 0.05;
      this.object.rotateY(-rotationAngle);
      // if (this.object.rotation.x<0.1){this.object.rotation.x += 0.0008; }
    }


    this.rotationAngle = 0.005 ;
    this.rotateX = 0;
  }
}