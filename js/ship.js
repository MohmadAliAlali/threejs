import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js';
import { Group } from 'three';
export default class Ship extends Group {
  constructor(name , position) {
    const loader = new GLTFLoader();
    super(); 
    this.name = 'land';
    this.position.y = position;
    loader.load(name, (gltf) => {
        this.add(gltf.scene);
    });
  }
}