import * as THREE from 'three';
import  Ship from './ship.js' ; 
import FirstPersonMovement from './controller.js';
import WaterEffect from './water.js';
import { calculateTotalForcesOnY, calculateTotalForcesOnX, calculateRotationAngle } from "../Physics/Physics2.js";
import { updateWavesEffect } from '../Physics/Waves.js';

const clock =new THREE.Clock();
const sceneContainer = document.getElementById('three-scene');
const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(80, window.innerWidth / window.innerHeight, 0.1, 15000);

const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth , window.innerHeight );
document.body.appendChild(renderer.domElement);
var light = new THREE.AmbientLight(0xfaffff , 0.5);
light.position.y = 170;
renderer.setClearColor(0x7ec0ee, 1);

const handleResize = () => {
    renderer.setSize(window.innerWidth,window.innerHeight);
};

window.addEventListener('resize',handleResize);

const waterEffect = new WaterEffect(scene, renderer, camera);
//get the glp model file and loder by ship.js file
const ship = new Ship('models/untitled.glb', 0);
ship.scale.set(10,10,10)

// ship.rotation.x = Math.PI/2;
const ice_place = new Ship('models/icescape_1_alien_moon_skybox.glb', 0);
ice_place.scale.set(160,160,160);
// ice_place.rotation.x = Math.PI/2;
const mouse = { x: 0, y: 0, movedThisFrame: false };

// Create an AudioListener attached to the camera
// Create an AudioListener attached to the camera
const listener = new THREE.AudioListener();
camera.add(listener);
// var axesHelper = new THREE.AxesHelper(500);
// scene.add(axesHelper);
// Create a global audio source
const seaWavesSound = new THREE.Audio(listener);
const seaWavesLoader = new THREE.AudioLoader();

// Load a sound and set it as the Audio object's buffer
seaWavesLoader.load('sounds/sea-waves-7131.mp3',function (buffer) {
    seaWavesSound.setBuffer(buffer);
    seaWavesSound.setLoop(false);
    seaWavesSound.setVolume(0.5);
    seaWavesSound.play();   
});

const shipSound = new THREE.Audio(listener);
const shipLoader = new THREE.AudioLoader();
shipLoader.load('sounds/ship-horn-very-close-14642.mp3', function (buffer) {
    shipSound.setBuffer(buffer);
    shipSound.setLoop(false); // Set to false for one-time play
    shipSound.setVolume(1);

    window.addEventListener('keydown', (event) => {
        if (event.code === 'Space') {
            // Resume the AudioContext
            listener.context.resume().then(() => {
                // Start playing the sound
                shipSound.play();
            });
        }else
        {
            seaWavesSound.play();
        }
    }); 
});



//add items and model to sceen
scene.add(light);
scene.add(ship);
// camera.scale.set(10,10,10);
ship.add(camera);
// ship.add(camera);


scene.add(ice_place);
camera.rotation.y = Math.PI/2;
const FBS = new FirstPersonMovement(ship);
renderer.domElement.onclick = function() {
    renderer.domElement.requestPointerLock();
};

renderer.domElement.onmousemove = function(e) {
    if (!mouse.movedThisFrame) {
        mouse.x = e.movementX;
        mouse.y = e.movementY;
        mouse.movedThisFrame = true;
    }
};

camera.position.y = ship.position.y + 10;
camera.position.x = ship.position.x + 50;
camera.position.z = ship.position.z +10;

let alpha = 0;
let incrementAlpha = false;
let decrementAlpha = false;

// Event listeners for key press
window.addEventListener('keydown', (event) => {
    if (event.key === 'ArrowLeft') {
        incrementAlpha = true; // Start incrementing alpha
    } else if (event.key === 'ArrowRight') {
        decrementAlpha = true; // Start decrementing alpha
    }
});

window.addEventListener('keyup', (event) => {
    if (event.key === 'ArrowLeft') {
        incrementAlpha = false; 
        alpha = 0;
    } else if (event.key === 'ArrowRight') {
        decrementAlpha = false; // Stop decrementing alpha
        alpha = 0;
    }
});

const animate = () => {
    setTimeout(function() {
        requestAnimationFrame(animate);
    }, 1000 / 60);

    const dt= clock.getDelta();
    waterEffect.water.material.uniforms.time.value += dt;

    const newX = calculateTotalForcesOnX(dt) * 0.01;
    const newY = calculateTotalForcesOnY(0.01)+ 15;
    ship.translateX(-newX);
    ship.position.y = newY;
    if (mouse.movedThisFrame) {
        camera.rotation.y -= mouse.x * 0.002;
    }
    if (incrementAlpha) {
        alpha = Math.min(alpha + 1, 45); // Increment alpha and cap at 90
    }
    if (decrementAlpha) {
        alpha = Math.max(alpha - 1, -45); // Decrement alpha and cap at -90
    }
    let rotationAngle = calculateRotationAngle(alpha);
    ship.rotation.y +=rotationAngle;
    
    const waveEffect = updateWavesEffect();
    ship.rotation.x = waveEffect.x;
    ship.rotation.z = waveEffect.z; 
    mouse.movedThisFrame = false;
    // Add any animations or interactions here
    renderer.render(scene, camera);
};



animate();