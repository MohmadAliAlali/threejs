import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import WaterEffect from './water';

export function loadAnimation(scene, path) {
  const loader = new GLTFLoader();

  loader.load(path, function(gltf) {
    const animation = gltf.scene;
    animation.position.y = 50;
    animation.scale.set(20,1,20)
    scene.add(animation);
  });
}