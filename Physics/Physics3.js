// Constants
const gravity = 9.81; // Gravitational constant (N·m²/kg²)
// Define the object properties
const ShipDraft = 16; //m
const ShipLength = 399; //m
const ShipWidth = 58.6; //m
const airArea = 4; // m^2
const waterArea = 12; // m^2
const Pitch=10; //m
const n = 100/60; //rps
const Vt=Pitch * n;
const Sr =0.15;
const Va = (1-Sr)*Vt //is the speed of advance (velocity of the propeller relative to the water).
const eta_P = 0.7  // Propulsive efficiency
const eta_H = 0.9  // Hull efficiency
// let isFloating = true;

// Define the properties of the medium
const airDensity = 1.225; // kg/m^3 (density of air)
const waterDensity = 1025; // kg/m^3 (density of water)





class Particle {
    constructor(mass, x, y, z) {
        this.mass = mass;
        this.x = x;//kg
        this.y = y;
        this.z = z;
        this.submergedVolume = ShipWidth*ShipDraft*ShipLength
        this.vx = 0; // Initial velocity
        this.vy = 0;
        this.vz = 0;
    }

    // Calculate gravitational force between this particle and Earth
    calculateForceFromEarth() {
        // Calculate the magnitude of the gravitational force
        const gravityForce = this.mass * gravity;
        // Calculate the force along the Y-axis
        return -gravityForce;
    }
    calculateBuoyancyForce(waterLevel) {
        // Calculate the submerged volume based on the particle's position
        const submergedDepth = Math.max(0, waterLevel - this.y);
        const submergedVolume = Math.min(ShipWidth * ShipLength * submergedDepth, this.submergedVolume);
    
        // Calculate the weight of the water displaced by the submerged part of the ship
        const displacedWaterWeight = waterDensity * submergedVolume * gravity;
        
        // Check if the water covers the surface of the ship
        // if (-15 >= this.y && isFloating) {
        //     // If water covers the ship, increase the weight by a factor (e.g., double the weight)
        //     this.mass = this.mass +(submergedVolume*waterDensity);
        //     isFloating =false// You can adjust this factor as needed
        // }
    
        // Calculate the buoyancy force equal to the weight of the displaced water
        const buoyancyForce = displacedWaterWeight;
    
        return buoyancyForce;
    }

    calculateAirDragForce() {
        const dragAirCoefficient = 0.6; // Drag coefficient for air
        // Calculate the magnitude of the drag force
        let dragForceMagnitude =
            0.5 * dragAirCoefficient * airDensity * airArea * Math.pow(this.vx, 2);
        return dragForceMagnitude;

    }
    calculateWaterDragForce() {
        let Re=(waterDensity*ShipLength*this.vx)/ (1.400e-3);//لزوجة المي
        let dragWaterCoefficient = 0.075/Math.pow((Math.log10(Re))-2,2); // Drag coefficient for water
        // Calculate the magnitude of the drag force
        let dragForceMagnitude =
            0.5 * dragWaterCoefficient * waterDensity * waterArea * Math.pow(this.vx, 2);
        return dragForceMagnitude;
    }
    calculateThrustForce(){
        const ThrustForce = (eta_P*3458000000)/(eta_H*Va)
        return ThrustForce
    }
}

// Create a buoy (floating object)
export const buoy = new Particle(206000000, 0, 0, 0);

export function calculateTotalForcesOnY(dt) {

    // Calculate gravitational force from Earth
    const fy = buoy.calculateForceFromEarth() + buoy.calculateBuoyancyForce(8);
    // Update accelerations
    const ay = fy / buoy.mass;
    // Update velocities
    buoy.vy += ay * dt;
    // Update positions (assuming y is the surface)
    buoy.y += buoy.vy * dt;
    // console.log(Va)
    // Adjust y position to stay above the surface
    return buoy.y;
}


export function calculateTotalForcesOnX(dt) {
    const fx = buoy.calculateThrustForce()-(buoy.calculateAirDragForce()+buoy.calculateWaterDragForce())
    // Update accelerations
    const ax = fx / buoy.mass;
    // Update velocities
    buoy.vx += ax * dt;

    buoy.x -= buoy.vx * dt;
    return buoy.x;
}
