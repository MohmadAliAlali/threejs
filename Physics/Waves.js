export function updateWavesEffect()
{
    const rotationSpeed = 0.001;
    const x = Math.sin(Date.now() * rotationSpeed) * 0.03;
    const z = Math.cos(Date.now() * rotationSpeed) * 0.005;
    return {x,z}
}