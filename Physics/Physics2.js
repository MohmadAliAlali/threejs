import Vector3 from './Vector3.js';

// Constants
const urlParams = new URLSearchParams(window.location.search);
const mass_val = parseFloat(urlParams.get('mass')) || 0; // Default value if mass is missing
const velocity_val = parseFloat(urlParams.get('velocity')) || 0  ; // Default value if velocity is missing

const gravity = 9.81; // m/s²
const ShipDraft = 16; // m
const ShipLength = 399; // m
const ShipWidth = 58.6; // m
const airArea = ShipLength * ShipDraft/2; // m², approximate frontal area
const waterArea = ShipWidth * ShipDraft/2; // m², approximate wetted area
const Pitch = 10; // m
const n = 100 / 60; // rps
const Vt = Pitch * n;
const Sr = 0.15;
const Va = (1 - Sr) * Vt;
const eta_P = 0.7; // Propulsive efficiency
const eta_H = 0.9; // Hull efficiency
const l = 10000000000; // Torque arm length
const d = ShipLength / 2; // Half-length of the ship

// Medium properties
const airDensity = 1.225; // kg/m³
const waterDensity = 1025; // kg/m³

class Particle {
    constructor(mass, position) {
        this.mass = mass;
        this.position = position;
        this.velocity = new Vector3(velocity_val, 0, 0); // Start stationary
        this.submergedVolume = ShipWidth * ShipDraft * ShipLength; // Maximum submerged volume
    }

    calculateForceFromEarth() {
        return new Vector3(0, -this.mass * gravity, 0);
    }

    calculateBuoyancyForce(waterLevel) {
        const submergedDepth = Math.max(0, waterLevel - this.position.y);
        const submergedVolume = Math.min(ShipWidth * ShipLength * submergedDepth, this.submergedVolume);
        const buoyancyForce = waterDensity * submergedVolume * gravity;
        return new Vector3(0, buoyancyForce, 0);
    }

    calculateAirDragForce() {
        const dragAirCoefficient = 0.5; // Adjusted for large ships
        const dragForceMagnitude = 0.5 * dragAirCoefficient * airDensity * airArea * Math.pow(this.velocity.x, 2);
        return new Vector3(-dragForceMagnitude, 0, 0);
    }

    calculateWaterDragForce() {
        const viscosity = 1.400e-3; // Dynamic viscosity of water
        const Re = (waterDensity * ShipLength * this.velocity.x) / viscosity;
        const dragWaterCoefficient = 0.075 / Math.pow(Math.log10(Re) - 2, 2);
        const dragForceMagnitude = 0.5 * dragWaterCoefficient * waterDensity * waterArea * Math.pow(this.velocity.x, 2);
        return new Vector3(-dragForceMagnitude, 0, 0);
    }

    calculateThrustForce() {
        const thrustPower = 8100000; // Adjusted power
        const ThrustForce = (eta_P * thrustPower) / (eta_H * Va);
        return new Vector3(ThrustForce, 0, 0);
    }

    calculateMoment(F_drag, alpha) {
        const alphaRadians = alpha* (Math.PI/180)// Convert angle to radians
        return F_drag * d * Math.sin(alphaRadians);
    }

    calculateNotMoment(F_drag, alpha) {
        const opposingAlphaRadians = (-alpha) *  (Math.PI/180) // Invert angle
        return F_drag * d * Math.sin(opposingAlphaRadians);
    }
}
// Create a buoy (floating object)
export const buoy = new Particle(206000000 + (mass_val*1000), new Vector3(0, 0, 0)); // Start at waterline

export function calculateTotalForcesOnY(dt) {
    const fy = buoy.calculateForceFromEarth().y + buoy.calculateBuoyancyForce(8).y;
    const ay = fy / buoy.mass;
    buoy.velocity.y += ay * dt;
    buoy.position.y += buoy.velocity.y * dt;
    return buoy.position.y;
}

export function calculateTotalForcesOnX(dt) {
    const fx = buoy.calculateThrustForce().x + buoy.calculateAirDragForce().x + buoy.calculateWaterDragForce().x;
    const ax = fx / buoy.mass;
    buoy.velocity.x += ax * dt;
    console.log(buoy.velocity)
    buoy.position.x += buoy.velocity.x * dt;
    return buoy.position.x;
}

export function calculateRotationAngle(alpha) {
    const F_drag =Math.abs(buoy.calculateWaterDragForce().x);
    const moment = buoy.calculateMoment(F_drag, alpha);
    const notMoment = buoy.calculateNotMoment(F_drag, alpha);
    const rotationAngle = (moment - notMoment) / l;
    return rotationAngle;
}