import { Vector3 } from 'three';
// Constants
const gravity = 9.81; // Gravitational constant (N·m²/kg²)
const dt = 0.01; // Time step (adjust as needed)
// Define the object properties
const ShipDraft = 16; //m
const ShipLength = 399; //m
const ShipWidth = 58.6; //m
const airArea = 4; // m^2
const waterArea = 12; // m^2
const Pitch=10; //m
const n = 100/60; //rps
const Vt=Pitch * n;
const Sr =0.15;
const Va = (1-Sr)*Vt //is the speed of advance (velocity of the propeller relative to the water).
const eta_P = 0.7  // Propulsive efficiency
const eta_H = 0.9  // Hull efficiency
// let isFloating = true;

// Define the properties of the medium
const airDensity = 1.225; // kg/m^3 (density of air)
const waterDensity = 1025; // kg/m^3 (density of water)





class Particle {
    constructor(mass, position) {
        this.mass = mass;
        this.position = position;
        this.velocity = new Vector3(0, 0, 0);
        this.submergedVolume = ShipWidth * ShipDraft * ShipLength;
    }

    calculateForceFromEarth() {
        return new Vector3(0, -this.mass * gravity, 0);
    }

    calculateBuoyancyForce(waterLevel) {
        const submergedDepth = Math.max(0, waterLevel - this.position.y);
        const submergedVolume = Math.min(ShipWidth * ShipLength * submergedDepth, this.submergedVolume);
        const buoyancyForce = waterDensity * submergedVolume * gravity;
        return new Vector3(0, buoyancyForce, 0);
    }

    calculateAirDragForce() {
        const dragAirCoefficient = 0.6;
        const dragForceMagnitude = 0.5 * dragAirCoefficient * airDensity * airArea * Math.pow(this.velocity.x, 2);
        return new Vector3(-dragForceMagnitude, 0, 0);
    }

    calculateWaterDragForce() {
        let Re = (waterDensity * ShipLength * this.velocity.x) / (1.400e-3);
        let dragWaterCoefficient = 0.075 / Math.pow(Math.log10(Re) - 2, 2);
        let dragForceMagnitude = 0.5 * dragWaterCoefficient * waterDensity * waterArea * Math.pow(this.velocity.x, 2);
        return new Vector3(-dragForceMagnitude, 0, 0);
    }

    calculateThrustForce() {
        const ThrustForce = (eta_P * 3458000000) / (eta_H * Va);
        return new Vector3(ThrustForce, 0, 0);
    }
}

// Create a buoy (floating object)
export const buoy = new Particle(206000000, new Vector3(0, 0, 0));

export function calculateTotalForcesOnY(dt) {
    const fy = buoy.calculateForceFromEarth().y + buoy.calculateBuoyancyForce(8).y;
    const ay = fy / buoy.mass;
    buoy.velocity.y += ay * dt;
    buoy.position.y += buoy.velocity.y * dt;
    return buoy.position.y;
}


export function calculateTotalForcesOnX(dt) {
    const fx = buoy.calculateThrustForce().x + (buoy.calculateAirDragForce().x + buoy.calculateWaterDragForce().x);
    const ax = fx / buoy.mass;
    buoy.velocity.x += ax * dt;
    buoy.position.x -= buoy.velocity.x * dt;
    return buoy.position.x;
}
